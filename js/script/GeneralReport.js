var respaldoData=[];
var respaldoDeposito=[];
var url='http://localhost/ServicesMovil/controlador.php';
$(document).ready(function(){
	  webshim.setOptions('forms-ext', {
      replaceUI: 'auto'
  });
webshim.polyfill('forms forms-ext');
verificar();
	$("#Datospersonal").hide();
	$("#reportCargo").hide();
	$('#mensaje').hide();
	$('#reportDispositivo').hide();
	$('#Abonar').hide();
	//alert('hola');
	$('#userr').html(sessionStorage.getItem('Nombre'));
	GetReporteGeneral();
	//alert(sessionStorage.getItem('UserId'));
	$("#l").on("change", verificarI);
	$("#lS").on("change", verificarS);
	
	$("#buscar").on("click", buscar);
	$("#buscarP").on("click",buscarDep);
});
function verificarI(){
	if ($("#fechaB").val().trim() !=='') {
		//alert('hola');
		$('#fechaB2').removeAttr('disabled');
	}else{
		$("#fechaB2").val('');
		$("#fechaB2").prop('disabled', true);
		//$('#fechaB2').Attr('disabled');
	}
}
function verificarS(){
	if ($("#fechaBP").val().trim() !=='') {
		//alert('hola');
		$('#fechaBP2').removeAttr('disabled');
	}else{
		$("#fechaBP2").val('');
		$("#fechaBP2").prop('disabled', true);
		//$('#fechaB2').Attr('disabled');
	}
}
function cerrarsesion(){
 sessionStorage.clear();
  window.location.href = 'index.html';
}
function verificar(){
	if (sessionStorage.getItem('logueado')==1) {
		//alert('sesion iniciadad');
	}else
	{
		sessionStorage.clear();
  		window.location.href = 'index.html';

		//alert('la sesion aun no se ha iniciado');
	}
}
function verifSaldo(Saldo,SaldoMin){
	if (Saldo==SaldoMin) {$("#saldazo").html("Usted cuenta con el minimo de saldo, favor de recargar para no perder los servicios");
		$("#Saldobajo").modal("toggle");
	}
	if (Saldo<SaldoMin) {$("#saldazo").html("Usted cuenta con el minimo de saldo, favor de recargar para no perder los servicios");
		$("#Saldobajo").modal("toggle");
		}
}
function GetReporteGeneral(){
	$("#Datospersonal").hide();
	$("#ReportG").show('slow');
	$("#reportCargo").hide();
	$('#reportDispositivo').hide();
	$('#Abonar').hide();
	  var datos={
		  	function:'generalReport',
		  	idCustomer:sessionStorage.getItem('CustomerID')
		  }
	$.ajax({
		url: url,
		type: 'POST',
		data:JSON.stringify(datos),
		//dataType:'json',
		success: function(data) {
			//alert(data);
			console.log(data);
		var datos=JSON.parse(data);
		//alert(datos[0]['NumSerial']);
		$('#Nserie').html(datos[0]['NumSerial']);
		$('#Tdispo').html(datos[0]['LastUpdate']);
		var Saldo=datos[0]['Balance'];
		var SaldoMin=datos[0]['BalanceMin'];
		$('#Saldo').html(datos[0]['Balance']);
		$('#Estado').html(datos[0]['MeterOpen']);
		$('#Battery').html(datos[0]['PowerSupply']);
		$('#BatteryB').html(datos[0]['BatteryBackup']);
		$('#BatteryS').html(datos[0]['BatteryServo']);
		$('#ServoB').html(datos[0]['BatteryServo']+'% De Carga');
		$('#ServoB').css({'width':datos[0]['BatteryServo']+'%'});
		$('#BackupB').html(datos[0]['BatteryBackup']+'% De Carga');
		$('#BackupB').css({'width':datos[0]['BatteryBackup']+'%'});
		//console.log(datos);
		verifSaldo(Saldo,SaldoMin);		
		},
		error: function(){
		alert('Error!');
		}
		});
}
function DatosPersonales(){
	$("#ReportG").hide();
	$("#reportCargo").hide();
	$("#Datospersonal").show('slow');
	$('#reportDispositivo').hide();
	$('#Abonar').hide();
	Get_setDatosP();
}

function Get_setDatosP(){
	var datos={
		function:'login',
		data:{
		contra:sessionStorage.getItem('contra'),
	 	correo:sessionStorage.getItem('correo')
		}
	};
	//console.log(JSON.stringify(datos));
	$.ajax({
		url: url,
		type: 'POST',
		data: JSON.stringify(datos),
		//dataType:'json',
		success: function(data) {
			console.log(data);
		var datos=JSON.parse(data);

		$('#nombre').html(datos.Names);
		$('#fecha').html(datos.RegisterDateCustomer);
		$('#name').attr('value', datos.Names);
		$('input#APP').attr('value',datos.PaternalName);
		$('#APM').attr('value',datos.MaternalName);
		$('#correo').attr('value',datos.Email);
		$('#Tlf').attr('value',datos.Telephone);
		$('#calle').attr('value',datos.Calle);
		$('#num').attr('value',datos.Numero);
		$('#cln').attr('value',datos.Colonia);
		$('#deleg').attr('value',datos.DelegacionoMuni);
		$('#estado').attr('value',datos.Estado);
		$('#muni').attr('value',datos.DelegacionoMuni);
		if(datos.Activo==1){$('#Estatus').attr('value','Activo');}
		$('#Balance').attr('value',datos.Balance);
		$('#FechaR').attr('value',datos.RegisterDateCustomer);
		//$('#RFC').attr('value',datos.RFC);
		$('#RFC').val(datos.RFC);
		$('#Rsocial').val(datos.razon_social);
		$('#DFisc').val(datos.direccion_fiscal);
		if (datos.factura==1) {
			$("#factura").prop("checked",true);

		}else{
			$("#factura").prop("checked",false);
		}
		},
		error: function(){
		alert('Error!');
		}
		});
}
function ReporteCargos(){
	$("#reportCargo").show('slow');
	$("#ReportG").hide();
	$("#Datospersonal").hide();
	$('#reportDispositivo').hide();
	$('#Abonar').hide();
	get_reportCargos()
}
function get_reportCargos(){
	

var datos={
		  	function:'reportecargos',
		  	idCustomer:sessionStorage.getItem('CustomerID')
		  }
	
	$.ajax({
		url: url,
		type: 'POST',
		data: JSON.stringify(datos),
		//dataType:'json',
		success: function(data) {
			//alert(data);
			 respaldoData=data;
			maquetar();
		
		
		},
		error: function(){
		//alert('Error!');
		}
		});
}
function maquetar(){
	dibujar();
	var b=[];
	var datos=JSON.parse(respaldoData);
			 
			 for(var i=0;i<1500;i++){
			 	//console.log(i);
			 	if(datos.length>i){
			 	   var u={Fecha: datos[i].RegisterDate,
             			 Lectura: datos[i].Lecture, 
               			Monto:datos[i].Amount
			 	
               		};
              
       			b[i]=u;
       			}
			 }
			 console.log(b);
			$('#Cargos').dynatable({dataset: {records: b}});
}
function maquetarDepositos(){
	dibujarDEP();
	var b=[];
	var datos=JSON.parse(respaldoDeposito);
			 
			 for(var i=0;i<1500;i++){
			 	//console.log(i);
			 	if (datos.length>i) {
			 		var u={Fecha: datos[i].RegisterDate,
             			 Lectura: datos[i].Lecture, 
               			Monto:datos[i].Amount
               			};
               			b[i]=u;
			 	}
			 	   
              
       			
			 }
			 //console.log(b);
			$('#Rdispositivo').dynatable({dataset: {records: b}});
}
function borrar(){
	$("#dynatable-pagination-links-Cargos").remove();
	$("#Cargos").remove();
	$("#dynatable-processing-Cargos").remove();
}
function borrarDep(){
	$("#dynatable-processing-Rdispositivo").remove();
	$("#Rdispositivo").remove();
	$("#dynatable-pagination-links-Rdispositivo").remove();
}
function dibujar(){
	borrar();
	var html='';
	html+='<table id="Cargos" class="dynatable table table-condensed table-hover table-striped table-bordered">'
		html+='<thead>'
			html+='<tr>'
				html+='<th data-dynatable-column="Fecha"><label>Fecha</label></th>'
				html+='<th data-dynatable-column="Lectura"><label>Lectura</label></th>'
				html+='<th data-dynatable-column="Monto"><label>Monto</label></th>'
			html+='</tr>'
		html+='</thead>'
		html+='<tbody>'
		html+='</tbody>'
	html+='</table>';
	$('#RCargos').append(html);
 
}
function dibujarDEP(){
	borrarDep();
	var html='';
	html+='<table id="Rdispositivo" class="dynatable table table-condensed table-hover table-striped table-bordered">'
		html+='<thead>'
			html+='<tr>'
				html+=' <th data-dynatable-column="Fecha"><label>Fecha</label></th>'
				html+=' <th data-dynatable-column="Monto"><label>Monto</label></th>'
			html+='</tr>'
		html+='</thead>'
		html+='<tbody>'
		html+='</tbody>'
	html+='</table>';
	$('#RDepositos').append(html);
 
}
function buscar(){
	var date=$('#fechaB').val();
	var date2=$('#fechaB2').val();
if ($("input#fechaB").val().trim() ==='' && $("input#fechaB2").val().trim()==='') {
alert('Los compos de las fechas no pueden ser vacios ');
}else{
	if ($("input#fechaB").val().trim() ===''&& $("input#fechaB2").val().trim()!=='') {
		//alert('alerta');
	var arrM=[];
	var arrA=[];
	var arrD=[];
	var encontrado=[];
	var cont=0;
	var b=[]; 
	fech=date.split("-");
	
	var Mes2=getDateVA(date2);
	
	Mes2        = Mes2.toUpperCase();
	
	 var data=JSON.parse(respaldoData);
	 for (var i=0;i<data.length;i++){
 	arrM[i] =data[i]['Mes'];
 	arrA[i]=data[i]['Anio'];
 	arrD[i]=data[i]['Dia'];
 	//var index=arrM[i].indexOf(Mes);
 	}
 	//alert(arrM.length);
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes2);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		//console.log(arrD[j].indexOf(fech[2]));
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}	

	dibujar();


	$('#Cargos').dynatable({dataset: {records: b}});
	}else{
		if ($("input#fechaB").val().trim() !=='' && $("input#fechaB2").val().trim()===''){
				var arrM=[];
	var arrA=[];
	var arrD=[];
	var encontrado=[];
	var cont=0;
	var b=[]; 
	fech=date.split("-");
	var Mes=getDateVA(date);
	
	Mes        = Mes.toUpperCase();
	
	 var data=JSON.parse(respaldoData);
	 for (var i=0;i<data.length;i++){
 	arrM[i] =data[i]['Mes'];
 	arrA[i]=data[i]['Anio'];
 	arrD[i]=data[i]['Dia'];
 	//var index=arrM[i].indexOf(Mes);
 	}
 	//console.log(arrM.length);
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}

	dibujar();


	$('#Cargos').dynatable({dataset: {records: b}});
	}else
		{
		if ($("input#fechaB").val().trim() !=='' && $("input#fechaB2").val().trim()!==''){
	var arrM=[];
	var arrA=[];
	var arrD=[];
	var encontrado=[];
	var cont=0;
	var b=[]; 
	fech=date.split("-");
	var Mes=getDateVA(date);
	var Mes2=getDateVA(date2);
	Mes        = Mes.toUpperCase();
	Mes2        = Mes2.toUpperCase();
	 var data=JSON.parse(respaldoData);
	 for (var i=0;i<data.length;i++){
 	arrM[i] =data[i]['Mes'];
 	arrA[i]=data[i]['Anio'];
 	arrD[i]=data[i]['Dia'];
 	//var index=arrM[i].indexOf(Mes);
 	}
 	//console.log(arrM.length);
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes2);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}	

	dibujar();
console.log(b);

	$('#Cargos').dynatable({dataset: {records: b}});
			}
		}
	}
}
cont=0;	
	
}
function buscarDep(){
	var date=$('#fechaBP').val();
	var date2=$('#fechaBP2').val();
	
if ($('#fechaBP').val().trim() ==='' && $('#fechaBP2').val().trim()==='') {
alert('Los compos de las fechas no pueden ser vacios ');
}else{
	if ($('#fechaBP').val().trim() ===''&& $('#fechaBP2').val().trim()!=='') {
		//alert('alerta');
	var arrM=[];
	var arrA=[];
	var arrD=[];
	var encontrado=[];
	var cont=0;
	var b=[]; 
	fech=date.split("-");
	
	var Mes2=getDateVA(date2);
	
	Mes2        = Mes2.toUpperCase();
	
	 var data=JSON.parse(respaldoDeposito);
	 for (var i=0;i<data.length;i++){
 	arrM[i] =data[i]['Mes'];
 	arrA[i]=data[i]['Anio'];
 	arrD[i]=data[i]['Dia'];
 	//var index=arrM[i].indexOf(Mes);
 	}
 	//alert(arrM.length);
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes2);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		//console.log(arrD[j].indexOf(fech[2]));
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}	

	dibujarDEP();


	$('#Rdispositivo').dynatable({dataset: {records: b}});
	}else{
		if ($('#fechaBP').val().trim() !=='' && $('#fechaBP2').val().trim()===''){
				var arrM=[];
	var arrA=[];
	var arrD=[];
	var encontrado=[];
	var cont=0;
	var b=[]; 
	fech=date.split("-");
	var Mes=getDateVA(date);
	
	Mes        = Mes.toUpperCase();
	
	 var data=JSON.parse(respaldoDeposito);
	 for (var i=0;i<data.length;i++){
 	arrM[i] =data[i]['Mes'];
 	arrA[i]=data[i]['Anio'];
 	arrD[i]=data[i]['Dia'];
 	//var index=arrM[i].indexOf(Mes);
 	}
 	//console.log(arrM.length);
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}

	dibujarDEP();


	$('#Rdispositivo').dynatable({dataset: {records: b}});
	}else
		{
		if ($('#fechaBP').val().trim() !=='' && $('#fechaBP2').val().trim()!==''){

	var arrM=[];
	var arrA=[];
	var arrD=[];
	var encontrado=[];
	var cont=0;
	var b=[]; 
	fech=date.split("-");
	var Mes=getDateVA(date);
	var Mes2=getDateVA(date2);
	Mes        = Mes.toUpperCase();
	Mes2        = Mes2.toUpperCase();
	 var data=JSON.parse(respaldoDeposito);
	 for (var i=0;i<data.length;i++){
 	arrM[i] =data[i]['Mes'];
 	arrA[i]=data[i]['Anio'];
 	arrD[i]=data[i]['Dia'];
 	//var index=arrM[i].indexOf(Mes);
 	}
 	alert(Mes2);
 	//console.log(arrM.length);
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes2);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}	

	dibujarDEP();
//console.log(b);

	$('#Rdispositivo').dynatable({dataset: {records: b}});
			}
		}
	}
}
cont=0;	
	
}
function buscarDep1(){
	var date=$('#fechaBP').val();
	var arrM=[];
	var arrA=[];
	var arrD=[];
	var encontrado=[];
	var cont=0;
	var b=[]; 
	fech=date.split("-");
	var Mes=getDateVA(date);
	Mes        = Mes.toUpperCase();
	 var data=JSON.parse(respaldoDeposito);
	 for (var i=0;i<data.length;i++){
 	arrM[i] =data[i]['Mes'];
 	arrA[i]=data[i]['Anio'];
 	arrD[i]=data[i]['Dia'];
 	//var index=arrM[i].indexOf(Mes);
 	}
 	console.log(arrM.length);
	for(var j=0;j<arrM.length;j++){
		var indexM=arrM[j].indexOf(Mes);
		var indexD=arrD[j].indexOf(fech[2]);
		var indexA=arrA[j].indexOf(fech[0]);
		
		if (indexM!==-1&&indexD!==-1&&indexA!==-1) {
			//console.log(data[j]);
			var u={Fecha: data[j].RegisterDate,
              Lectura: data[j].Lecture, 
               Monto:data[j].Amount
               };
              
       			b[cont]=u;
			//encontrado[j]=data[j];
			cont=cont+1;
			//console.log("elemento Encontrado"+data[j]);
		}else{
		//console.log('elemento no Encontrado');
		}
	}	
	dibujarDEP();
//console.log(b);

	$('#Rdispositivo').dynatable({dataset: {records: b}});
	//console.log(arr);
	//alert(arr.indexOf("AGOSTO"));
}
function getDateVA(fechas){
   var fecha= fechas.split("-");
 var Mes='Mes';
    var ano=fecha[0];
       /*if(fecha[1] == "01"){Mes=fecha[2]+' Enero '+ano;} 
       if(fecha[1] == "02"){Mes=fecha[2]+' Febrero '+ano;} 
       if(fecha[1] == "03"){Mes=fecha[2]+' Marzo '+ano;} 
       if(fecha[1] == "04"){Mes=fecha[2]+' Abril '+ano;} 
       if(fecha[1] == "05"){Mes=fecha[2]+' Mayo '+ano;} 
       if(fecha[1] == "06"){Mes=fecha[2]+' Junio '+ano;} 
       if(fecha[1] == "07"){Mes=fecha[2]+' Julio '+ano;} 
       if(fecha[1] == "08"){Mes=fecha[2]+' Agosto '+ano;} 
       if(fecha[1] == "09"){Mes=fecha[2]+' Septiembre '+ano;}
       if(fecha[1] == "10"){Mes=fecha[2]+' Octubre '+ano;} 
       if(fecha[1] == "11"){Mes=fecha[2]+' Noviembre '+ano;} 
       if(fecha[1] == "12"){Mes=fecha[2]+' Diciembre '+ano;} */
       if(fecha[1] == "01"){Mes='Enero';} 
       if(fecha[1] == "02"){Mes='Febrero';} 
       if(fecha[1] == "03"){Mes='Marzo';} 
       if(fecha[1] == "04"){Mes='Abril';} 
       if(fecha[1] == "05"){Mes='Mayo';} 
       if(fecha[1] == "06"){Mes='Junio';} 
       if(fecha[1] == "07"){Mes='Julio';} 
       if(fecha[1] == "08"){Mes='Agosto';} 
       if(fecha[1] == "09"){Mes='Septiembre';}
       if(fecha[1] == "10"){Mes='Octubre';} 
       if(fecha[1] == "11"){Mes='Noviembre';} 
       if(fecha[1] == "12"){Mes='Diciembre';}  

return Mes;
}
function updPassword(){
	var datos={
		  	function:'changePassword',
		  	data:{
		  		idUser:sessionStorage.getItem('UserId'),
		  		password:$('#Password').val()
		  	}
		  	
		  };
if($('#Password').val()!=$('#CpassWord').val()){
  $('#mensaje').html('las contraseñas no coinciden');
                $('#mensaje').show("slow",function() {
                    //alert ('imagen mostrada!');
                   setTimeout(function() {
                   $("#mensaje").fadeOut(1000);
                    },3000);
                     $('#Password').val('');
                     $('#CpassWord').val('');
                  });
}else{


	$.ajax({
		url: url,
		type: 'POST',
		data: JSON.stringify(datos),
		//dataType:'json',
		success: function(data) {
			//alert(data);
			if (data=='true') {				
				$('#mensaje').html('Su Contraseña se Modifico');
                $('#mensaje').show("slow",function() {
                    //alert ('imagen mostrada!');
                   setTimeout(function() {
                   $("#mensaje").fadeOut(800);

                   $('#myModal').modal('hide');
                    },3000);
                     $('#Password').val('');
                     $('#CpassWord').val('');
                  });
				
			}
			console.log(data);
			//var datos=JSON.parse(data);
	
		
		},
		error: function(){
		console.log('Error!');
		}
		});
	}		
}
function paymentReport(){
	$("#reportCargo").hide()
	$("#ReportG").hide();
	$("#Datospersonal").hide();
	$('#reportDispositivo').show('slow');
	$('#Abonar').hide();
	get_paymentReport();
}
function get_paymentReport(){
	var b=[]

	var datos={
		  	function:'paymentReport',
		  	idCustomer:sessionStorage.getItem('CustomerID')
		  }
	
	$.ajax({
		url: url,
		type: 'POST',
		data: JSON.stringify(datos),
		//dataType:'json',
		success: function(data) {
			//alert(data);
			//console.log(data);
			respaldoDeposito=data;
			maquetarDepositos();
		},
		error: function(){
		alert('Error!');
		}
		});
}
function updateBalance(){
	$("#reportCargo").hide()
	$("#ReportG").hide();
	$("#Datospersonal").hide();
	$('#reportDispositivo').hide();
	$('#Abonar').show('slow');
}
function set_Balance(){
	var datos={
		  	function:'updateBalance',
		  	idDevice:sessionStorage.getItem('CusDeviceID'),
		  	idCustomer:sessionStorage.getItem('CustomerID'),
		  	Amount:$('#Monto').val()
		  };
		  $.ajax({
		url: url,
		type: 'POST',
		data: JSON.stringify(datos),
		//dataType:'json',
		success: function(data) {
			//alert(data);
			//console.log(data);
			//var datos=JSON.parse(data);
			if (data=="true") {
				$('#notif').html('su recarga se realizo de manera exitosa');
                $('#notif').show("slow",function() {
                   setTimeout(function() {
                   $("#notif").fadeOut(1500);
                    },3000);
                    $(":text").each(function(){ 
                        $($(this)).val('');
                	});
                	$('#fecha').val('');
                  });
			}
			
		
		},
		error: function(){
		alert('Error!');
		}
		});

}
function updateUsers(){
	 if( $('#factura').prop('checked') ) {
         var merchant="1";
    //alert('Seleccionado');
    }else{
       var merchant="0";
    }
	var datos={
		  	function:'updateUsers',
		  	data:{
				rfc:$('#RFC').val(),
				RSocial:$('#Rsocial').val(),
				DFiscal:$('#DFisc').val(),
				factura:merchant,
				correo:sessionStorage.getItem('correo')
				}
		  };
		  console.log(JSON.stringify(datos));
	$.ajax({
		url: url,
		type: 'POST',
		data: JSON.stringify(datos),
		//dataType:'json',
		success: function(data) {

			//alert(data);
			//console.log(data);
			if (data==1) {
				$('#notifA').html('Datos Actualizados');
                $('#notifA').show("slow",function() {
                   setTimeout(function() {
                   $("#notifA").fadeOut(1500);
                    },3000);
                    $(":text").each(function(){ 
                        $($(this)).val('');
                	});
                	$("#factura").prop("checked",false);
                	/*$('#RFC').val();
                	$('#Rsocial').val();
                	$('#DFisc').val();*/
                  });
			}
		
		},
		error: function(){
		alert('Error!');
		}
		});
}