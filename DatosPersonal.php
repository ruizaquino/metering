<?php
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

} else {
   echo "Esta pagina es solo para usuarios registrados.<br>";
   echo "<br><a href='login.html'>Login</a>";
   echo "<br><br><a href='index.html'>Registrarme</a>";

exit;
}

$now = time();

if($now > $_SESSION['expire']) {
session_destroy();

echo "Su sesion a terminado,
<a href='login.html'>Necesita Hacer Login</a>";
exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Datos Personales</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SG METERING</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>Usuario</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>Usuario</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>Usaurio</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Leer todos los mensajes</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                     <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="#">Alerta <span class="label label-default">Alerta Saldo</span></a>
                            </li>
                            <li>
                                <a href="#">Alerta <span class="label label-primary">Alerta Dispositivo</span></a>
                            </li>
                            <li>
                                <a href="#">Alerta <span class="label label-success">Alerta Consumo</span></a>
                            </li>
                            <li>
                                <a href="#">Alerta<span class="label label-info">Reporte</span></a>
                            </li>
                         
                            <li class="divider"></li>
                            <li>
                                <a href="#">Mostrar Todas</a>
                            </li>
                        </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="userr" ><i class="fa fa-user"></i> USaurio <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Perfil</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Configuracion</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i>Cerrar Sesion</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="https://localhost/startbootstrap/inicio.php?control=true&idCustomer=14"><i class="fa fa-fw fa-dashboard"></i> Reporte General</a>
                    </li>
                    <li>
                        <a href="#" onclick="datosPersonal()"><i class="fa fa-fw fa-bar-chart-o"></i> Datos Personales</a>
                    </li>
                    <li>
                      <a data-toggle="modal" href="#myModal"><i class="fa fa-fw fa-table"></i> Cambiar Contraseña</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-edit"></i> Mi Dispositivo</a>
                    </li>
                    <li>
                        <a href="#" onclick="ReportesCargos()"><i class="fa fa-fw fa-desktop"></i> Reporte de Cargos</a>
                    </li>
                    <li>
                        <a  data-toggle="modal" href="#depositoModal"><i class="fa fa-fw fa-wrench"></i>Reporte de Depositos</a>
                    </li>
                    
                    <li>
                        <a href="blank-page.php"><i class="fa fa-fw fa-file"></i> Abonar Saldo</a>
                    </li>
                    <li>
                       <!-- <a href="index-rtl.html"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>-->
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Datos Generales del Usuario
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="inicio.html">INICIO</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-bar-chart-o"></i> Informacion General 
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <!-- Flot Charts -->
               <!-- <div class="row">
                    <div class="col-lg-12">
                        Esta es la primera seccion el primer row
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                        
                            <div class="col-lg-1"></div>
                              <div class="col-lg-2">
                               <i class="fa fa-user fa-3x"></i>
                              </div>
                            <div class="col-lg-4">
                            <form role="form">
                                <div class="form-group">
                                <label id="nombre">Usuario</label>
                                </div>
                                
                            </form>
                            </div>
                             <div class="col-lg-4">
                              <form role="form">
                                    <div class="form-group">
                                        <label id="fecha">Fecha de registro</label>
                                    </div>  
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-1"></div>

                    <div class="col-lg-10">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                               <!-- <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Multiple Axes Line Graph Example with Tooltips and Raw Data</h3>-->
                               Informacion personal
                            </div>
                            <div class="panel-body">
                                <div class="flot-chart">
                                  <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#usuario">Datos Usuario</a></li>
                                <li><a data-toggle="tab" href="#direccion">Direccion</a></li>
                                <li><a data-toggle="tab" href="#datoscuenta">Datos Cuenta</a></li>
                                <li><a data-toggle="tab" href="#datosfiscales">Datos Fiscales</a></li>
                              
                              </ul>
                              <div class="tab-content">
                                <div id="usuario" class="tab-pane fade in active">
                                  <h3>Datos Usuario</h3>
                                  <div class="col-lg-1"></div>
                                  <div class="col-lg-5"> 
                                                              
                                   <form role="form">
                                      <div class="form-group">
                                        <label for="name">Nombre:</label>
                                        <input type="name" class="form-control" id="name">
                                      </div>
                                      <div class="form-group">
                                        <label for="APP">Apellido Paterno:</label>
                                        <input type="name" class="form-control" id="APP">
                                      </div>
                                      <div class="form-group">
                                        <label for="APM">Apellido Materno:</label>
                                        <input type="text" class="form-control" id="APM">
                                      </div>
                                    </form>
                                    </div>
                                    <div class="col-lg-5">                             
                                   <form role="form">
                                      
                                      <div class="form-group">
                                        <label for="correo">Correo:</label>
                                        <input type="email" class="form-control" id="correo">
                                      </div>
                                      <div class="form-group">
                                        <label for="Tlf">Telefono:</label>
                                        <input type="text" class="form-control" id="Tlf">
                                      </div>
                                     
                                    </form>
                                    </div>
                                </div>
                                <div id="direccion" class="tab-pane fade">
                                  <h3>Direccion</h3>
                                  <div class="col-lg-1"></div>
                                  <div class="col-lg-5">                             
                                   <form role="form">
                                      <div class="form-group">
                                        <label for="calle">Calle:</label>
                                        <input type="name" class="form-control" id="calle">
                                      </div>
                                      <div class="form-group">
                                        <label for="num">Numero:</label>
                                        <input type="name" class="form-control" id="num">
                                      </div>
                                      <div class="form-group">
                                        <label for="cln">Colonia:</label>
                                        <input type="text" class="form-control" id="cln">
                                      </div>
                                    </form>
                                    </div>
                                    <div class="col-lg-5">                             
                                   <form role="form">
                                      <div class="form-group">
                                        <label for="deleg">Delegacion:</label>
                                        <input type="text" class="form-control" id="deleg">
                                      </div>
                                      <div class="form-group">
                                        <label for="estado">Estado:</label>
                                        <input type="email" class="form-control" id="estado">
                                      </div>
                                      <div class="form-group">
                                        <label for="muni">Municipio:</label>
                                        <input type="text" class="form-control" id="muni">
                                      </div>
                                     
                                    </form>
                                    </div>
                                </div>
                                <div id="datoscuenta" class="tab-pane fade">
                                  <h3>Datos Cuenta</h3>
                                  <div class="col-lg-3"></div>
                                  <div class="col-lg-6">                             
                                   <form role="form">
                                      <div class="form-group">
                                        <label for="name">Estatus:</label>
                                        <input type="name" class="form-control" id="Estatus">
                                      </div>
                                      <div class="form-group">
                                        <label for="APP">Saldo:</label>
                                        <input type="name" class="form-control" id="Balance">
                                      </div>
                                      <div class="form-group">
                                        <label for="APM">Fecha de Registro:</label>
                                        <input type="text" class="form-control" id="FechaR">
                                      </div>
                                    </form>
                                    </div>
                                    
                                </div>

                                <div id="datosfiscales" class="tab-pane fade">
                                  <h3>Datos Fiscales</h3>
                                  <div class="col-lg-3"></div>
                                  <div class="col-lg-6">                             
                                   <form role="form">
                                        <div class="form-group">
                                            <label for="RFC">RFC:</label>
                                            <input type="text" class="form-control" id="RFC">
                                         </div>

                                      <div class="form-group">
                                        <label for="name">Razon Social:</label>
                                        <input type="name" class="form-control" id="Rsocial">
                                      </div>
                                      <div class="form-group">
                                        <label for="APP">Direccion Fiscal:</label>
                                        <input type="name" class="form-control" id="DFisc">
                                      </div>
                                      <div class="form-group">
                                        <label for="APM">Generar Factura:</label>
                                        <div class="checkbox">
                                          <label><input type="checkbox" value="">Facturacion </label>
                                        </div>
                                      </div>
                                    </form>
                                </div>
                                   <div class="row">
                                       <div class="col-lg-9"></div>
                                       <div class="col-lg-3">
                                            <button type="button" class="btn btn-warning">Cancelar</button>
                                             <button type="button" class="btn btn-success">Actualizar</button>
                                       </div>
                                   </div> 
                                </div>
                              </div>
                                </div>
                                <div class="text-right">
                                    <!--<a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

              
                <!-- /.row -->

                <!-- Morris Charts -->
             
                <!-- /.row -->

                
                <!-- /.row -->

                
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
     <!-- Modal para cambio de contraseña -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cambiar Contraseña</h4>
        </div>
        <div class="modal-body">
          <form role="form">
                 <div class="form-group">
                 <label for="name">Contraseña actual:</label>
                 <input type="password" class="form-control" id="name">
                </div>
                 <div class="form-group">
                 <label for="APP">Nueva Contraseña:</label>
                  <input type="password" class="form-control" id="APP">
                </div>
                <button type="submit" class="btn btn-default">Cambiar</button>
            </form>



        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<!-- Modal para reportes de Depositos -->
  <div class="modal fade" id="depositoModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cambiar Contraseña</h4>
        </div>
        <div class="modal-body">
         <div class="container-fluid bd-example-row">
                <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4"><form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input type="search"  class="form-control" placeholder="Buscar">
                            </div>
                            <button type="submit" align="center" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                         </form>
                    </div>
                </div>
               
            </div>
            <div class="table-responsive">
                         <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Monto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="active">
                                        <td>Junio-23-2016</td>
                                        <td>$0.29</td>
                                        
                                    </tr>
                                    <tr class="success">
                                         <td>Junio-23-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr class="warning">
                                         <td>Junio-23-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr class="danger">
                                         <td>Junio-22-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                         <td>Junio-22-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                         <td>Junio-22-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                        <td>Junio-22-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                </tbody>
                            </table>
            </div>



        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
     </div>
  </div>
    <!-- jQuery -->
  
<script src="https://localhost/startbootstrap/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
   
     <script src="https://localhost/startbootstrap/js/script/script.js"></script>
    <script src="https://localhost/startbootstrap/js/bootstrap.min.js"></script>
    <script src="https://localhost/startbootstrap/js/script/script.js"></script>

    <!-- Morris Charts JavaScript -->
   <!-- <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>-->

    <!-- Flot Charts JavaScript -->
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
    <!--<script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/flot-data.js"></script>-->

</body>

</html>
