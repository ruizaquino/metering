<?php
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

} else {
   echo "Esta pagina es solo para usuarios registrados.<br>";
   echo "<br><a href='login.html'>Login</a>";
   echo "<br><br><a href='index.html'>Registrarme</a>";

exit;
}

$now = time();

if($now > $_SESSION['expire']) {
session_destroy();

echo "Su sesion a terminado,
<a href='index.html'>Necesita Hacer Login</a>";
exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Metering-Reporte de cargos</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="inicio.php">REPORTE DE CARGOS</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>USUARIO</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>USUARIO</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>USUARIO</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Leer todos los mensajes</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                  <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alerta <span class="label label-default">Alerta Saldo</span></a>
                        </li>
                        <li>
                            <a href="#">Alerta <span class="label label-primary">Alerta Dispositivo</span></a>
                        </li>
                        <li>
                            <a href="#">Alerta <span class="label label-success">Alerta Consumo</span></a>
                        </li>
                        <li>
                            <a href="#">Alerta<span class="label label-info">Reporte</span></a>
                        </li>
                     
                        <li class="divider"></li>
                        <li>
                            <a href="#">Mostrar Todas</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Usuario<b class="caret"></b></a>
                   <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Perfil</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Configuracion</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i>Cerrar Sesion</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="inicio.php"><i class="fa fa-fw fa-dashboard"></i> Reporte General</a>
                    </li>
                    <li>
                        <a href="DatosPersonal.php"><i class="fa fa-fw fa-bar-chart-o"></i> Datos Personales</a>
                    </li>
                    <li>
                        <a data-toggle="modal" href="#myModal"><i class="fa fa-fw fa-table"></i> Cambiar Contraseña</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-edit"></i> Mi Dispositivo</a>
                    </li>
                    <li>
                        <a href="tables.php"><i class="fa fa-fw fa-desktop"></i> Reporte de Cargos</a>
                    </li>
                    <li>
                         <a  data-toggle="modal" href="#depositoModal"><i class="fa fa-fw fa-wrench"></i>Reporte de Depositos</a>
                    </li>
                    <!--<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>-->
                    <li>
                        <a href="blank-page.php"><i class="fa fa-fw fa-file"></i> Abonar Saldo</a>
                    </li>
                    <li>
                       <!-- <a href="index-rtl.html"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>-->
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            REPORTE DE CARGOS
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">INICIO</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Tabla de reportes 
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
               
                <!--esta es una seccion de un row-->
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8">
                        
                    </div>
                      <div class="col-lg-4">
                         <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Buscar">
                            </div>
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </form>
                    </div>
                </div>
             <!--esta es una seccion de un row-->
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-1">
                        
                    </div>
                    <div class="col-lg-10">
                        <h2>Reporte General de consumo y cargos</h2>
                        <div class="table-responsive">
                            <table id='reporteCargos' class="display" cellspacing="0" width="100%"">
                                <thead>
                                    
                                     <tr>
                                      <td><strong>#Fecha</strong></td>
                                      <td><strong>Consumo</strong></td>
                                      <td><strong>Monto</strong></td>
                                      
                                     </tr>                                   
                                </thead>
                                <tbody>
                                
                                    <!--<tr class="active">
                                        <td>Junio-23-2016</td>
                                        <td>00000.01 m3</td>
                                        <td>$0.29</td>
                                        
                                    </tr>
                                    <tr class="success">
                                         <td>Junio-23-2016</td>
                                        <td>00000.01 m3</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr class="warning">
                                         <td>Junio-23-2016</td>
                                        <td>00000.01 m3</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr class="danger">
                                         <td>Junio-22-2016</td>
                                        <td>00000.01 m3</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                         <td>Junio-22-2016</td>
                                        <td>00000.01 m3</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                         <td>Junio-22-2016</td>
                                        <td>00000.01 m3</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                        <td>Junio-22-2016</td>
                                        <td>00000.01 m3</td>
                                        <td>$0.29</td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
     <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cambiar Contraseña</h4>
        </div>
        <div class="modal-body">
          <form role="form">
                 <div class="form-group">
                 <label for="name">Contraseña actual:</label>
                 <input type="password" class="form-control" id="name">
                </div>
                 <div class="form-group">
                 <label for="APP">Nueva Contraseña:</label>
                  <input type="password" class="form-control" id="APP">
                </div>
                <button type="submit" class="btn btn-default">Cambiar</button>
            </form>



        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<!-- Modal para reportes de Depositos -->
  <div class="modal fade" id="depositoModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cambiar Contraseña</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid bd-example-row">
                <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4"><form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input type="search"  class="form-control" placeholder="Buscar">
                            </div>
                            <button type="submit" align="center" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                         </form>
                    </div>
                </div>
               
            </div>
            <div class="table-responsive">
                         <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Monto</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="active">
                                        <td>Junio-23-2016</td>
                                        <td>$0.29</td>
                                        
                                    </tr>
                                    <tr class="success">
                                         <td>Junio-23-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr class="warning">
                                         <td>Junio-23-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr class="danger">
                                         <td>Junio-22-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                         <td>Junio-22-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                         <td>Junio-22-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                    <tr>
                                        <td>Junio-22-2016</td>
                                        <td>$0.29</td>
                                    </tr>
                                </tbody>
                            </table>
            </div>



        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
    <!-- jQuery -->
    <script src="https://localhost/startbootstrap/js/jquery.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="https://localhost/startbootstrap/js/bootstrap.min.js"></script>
    <script src="https://localhost/startbootstrap/js/script/script.js"></script>

    <!-- Bootstrap Core JavaScript -->
   

</body>

</html>
